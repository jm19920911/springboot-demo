package jm.springboot.demo.service.impl;

import jm.springboot.demo.mapper.MemberMapper;
import jm.springboot.demo.model.Member;
import jm.springboot.demo.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author jm
 */
@Service
@Transactional()
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberMapper memberMapper;

    @Override
    public Member getById(Integer id) {
        return memberMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Member> getByCondition(Member condition) {
        return memberMapper.select(condition);
    }

    @Override
    public List<Member> getAll() {
        return memberMapper.selectAll();
    }
}
